DROP database if exists coloc;

create database coloc;
use coloc;

create table Colocataire(
    id int(11) unsigned not null auto_increment,
    nom VARCHAR(50),
    prenom VARCHAR(50),
    mail VARCHAR(50),
    telephone VARCHAR(20),

    primary key(id)
);


create table Depense(
    id int unsigned not null auto_increment,
    idColocataire int(11) unsigned NOT NULL,
    Datedepense DateTime,
    Text VARCHAR(50),
    justificatif VARCHAR(256),
    montant numeric(10,2),
    reparti Boolean,

    primary key(id),
    FOREIGN key(idColocataire) REFERENCES Colocataire(id)
);
