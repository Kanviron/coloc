﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Model;
using Coloc.Dao;


namespace Coloc.View
{
    public partial class FConsultColoc : Form

    {

        public FConsultColoc()
        {
            InitializeComponent();

            this.btSupprimer.Click += BtSupprimer_Click;
            this.btSave.Click += BtSave_Click;
            this.btnModifier.Click += BtnModifier_Click;
            this.btAjout.Click += BtAjout_Click;
            this.load(new DaoColocataire().GetAll());
        }

        private void BtAjout_Click(object sender, EventArgs e)
        {
            FAjoutcoloc fmodif = new FAjoutcoloc(lbConsultColoc);
            fmodif.Show();
        }

        private void BtnModifier_Click(object sender, EventArgs e)
        {
            if (lbConsultColoc.SelectedIndex == -1) return;
            int position = lbConsultColoc.SelectedIndex;
            FModifiercoloc fmodif = new FModifiercoloc(State.modified, lbConsultColoc.Items,position);
            fmodif.Show();
        }

        private void BtSave_Click(object sender, EventArgs e)
        {
            List<Colocataire> colocs = new List<Colocataire>();
            foreach (object o in lbConsultColoc.Items)
            {
                colocs.Add((Colocataire)o);
            }
            new DaoColocataire().SaveChanges(colocs);
            this.load(colocs);
        }

        private void load(List<Colocataire> colocataires)
        {
            lbConsultColoc.Items.Clear();
            foreach (Colocataire c in colocataires)
            {
                lbConsultColoc.Items.Add(c);
            }
        }

        private void BtSupprimer_Click(object sender, EventArgs e)
        {
            if (lbConsultColoc.SelectedIndex == -1) return;
            int position = lbConsultColoc.SelectedIndex;
            ((Colocataire)lbConsultColoc.Items[position]).Remove();
            lbConsultColoc.Items[position] = lbConsultColoc.Items[position];
        }


        private void FConsultColoc_Load(object sender, EventArgs e)
        {
            
        }

        private void btnValider_Click(object sender, EventArgs e)
        {
            
        }
    }
}
