﻿
namespace Coloc.View
{
    partial class FModifiercoloc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dd = new System.Windows.Forms.Label();
            this.btModifier = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tbModifieremail = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbModifierprenom = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbModifiertéléphone = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbModifierNom = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // dd
            // 
            this.dd.AutoSize = true;
            this.dd.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dd.Location = new System.Drawing.Point(55, 33);
            this.dd.Name = "dd";
            this.dd.Size = new System.Drawing.Size(255, 26);
            this.dd.TabIndex = 37;
            this.dd.Text = "Modifier des colocataires";
            // 
            // btModifier
            // 
            this.btModifier.Location = new System.Drawing.Point(102, 320);
            this.btModifier.Name = "btModifier";
            this.btModifier.Size = new System.Drawing.Size(128, 36);
            this.btModifier.TabIndex = 36;
            this.btModifier.Text = "Modifier";
            this.btModifier.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AccessibleRole = System.Windows.Forms.AccessibleRole.Sound;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 254);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 22);
            this.label4.TabIndex = 35;
            this.label4.Text = "Modifier email";
            // 
            // tbModifieremail
            // 
            this.tbModifieremail.Location = new System.Drawing.Point(228, 254);
            this.tbModifieremail.Name = "tbModifieremail";
            this.tbModifieremail.Size = new System.Drawing.Size(119, 20);
            this.tbModifieremail.TabIndex = 34;
            // 
            // label3
            // 
            this.label3.AccessibleRole = System.Windows.Forms.AccessibleRole.Sound;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 203);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 22);
            this.label3.TabIndex = 33;
            this.label3.Text = "Modifier le prenom";
            // 
            // tbModifierprenom
            // 
            this.tbModifierprenom.Location = new System.Drawing.Point(228, 206);
            this.tbModifierprenom.Name = "tbModifierprenom";
            this.tbModifierprenom.Size = new System.Drawing.Size(119, 20);
            this.tbModifierprenom.TabIndex = 32;
            // 
            // label5
            // 
            this.label5.AccessibleRole = System.Windows.Forms.AccessibleRole.Sound;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(188, 22);
            this.label5.TabIndex = 31;
            this.label5.Text = "Modifier le Téléphone";
            // 
            // tbModifiertéléphone
            // 
            this.tbModifiertéléphone.Location = new System.Drawing.Point(228, 151);
            this.tbModifiertéléphone.Name = "tbModifiertéléphone";
            this.tbModifiertéléphone.Size = new System.Drawing.Size(119, 20);
            this.tbModifiertéléphone.TabIndex = 30;
            // 
            // label2
            // 
            this.label2.AccessibleRole = System.Windows.Forms.AccessibleRole.Sound;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 22);
            this.label2.TabIndex = 29;
            this.label2.Text = "Modifier le Nom";
            // 
            // tbModifierNom
            // 
            this.tbModifierNom.Location = new System.Drawing.Point(228, 106);
            this.tbModifierNom.Name = "tbModifierNom";
            this.tbModifierNom.Size = new System.Drawing.Size(119, 20);
            this.tbModifierNom.TabIndex = 28;
            // 
            // FModifiercoloc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 450);
            this.Controls.Add(this.dd);
            this.Controls.Add(this.btModifier);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbModifieremail);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbModifierprenom);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbModifiertéléphone);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbModifierNom);
            this.Name = "FModifiercoloc";
            this.Text = "FfenetreAjout";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label dd;
        private System.Windows.Forms.Button btModifier;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbModifieremail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbModifierprenom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbModifiertéléphone;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbModifierNom;
    }
}