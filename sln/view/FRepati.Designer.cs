﻿
namespace Projet_Colocation
{
    partial class Repartition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSolder = new System.Windows.Forms.Button();
            this.lbRepartition = new System.Windows.Forms.Label();
            this.lbox_Repartition = new System.Windows.Forms.ListBox();
            this.btnMenu = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSolder
            // 
            this.btnSolder.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnSolder.Location = new System.Drawing.Point(315, 307);
            this.btnSolder.Name = "btnSolder";
            this.btnSolder.Size = new System.Drawing.Size(118, 25);
            this.btnSolder.TabIndex = 46;
            this.btnSolder.Text = "Solder les dépenses";
            this.btnSolder.UseVisualStyleBackColor = true;
            this.btnSolder.Click += new System.EventHandler(this.btnSolder_Click);
            // 
            // lbRepartition
            // 
            this.lbRepartition.AutoSize = true;
            this.lbRepartition.Location = new System.Drawing.Point(79, 33);
            this.lbRepartition.Name = "lbRepartition";
            this.lbRepartition.Size = new System.Drawing.Size(290, 13);
            this.lbRepartition.TabIndex = 47;
            this.lbRepartition.Text = "Récapitulatif de la répartition des dépenses des colocataires";
            // 
            // lbox_Repartition
            // 
            this.lbox_Repartition.FormattingEnabled = true;
            this.lbox_Repartition.HorizontalScrollbar = true;
            this.lbox_Repartition.Location = new System.Drawing.Point(82, 64);
            this.lbox_Repartition.Name = "lbox_Repartition";
            this.lbox_Repartition.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbox_Repartition.Size = new System.Drawing.Size(599, 225);
            this.lbox_Repartition.TabIndex = 49;
            // 
            // btnMenu
            // 
            this.btnMenu.Location = new System.Drawing.Point(581, 27);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(100, 25);
            this.btnMenu.TabIndex = 48;
            this.btnMenu.Text = "Menu";
            this.btnMenu.UseVisualStyleBackColor = true;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // Repartition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lbox_Repartition);
            this.Controls.Add(this.btnMenu);
            this.Controls.Add(this.lbRepartition);
            this.Controls.Add(this.btnSolder);
            this.Name = "Repartition";
            this.Text = "FRepati";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSolder;
        private System.Windows.Forms.Label lbRepartition;
        private System.Windows.Forms.ListBox lbox_Repartition;
        private System.Windows.Forms.Button btnMenu;
    }
}