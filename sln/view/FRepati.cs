﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Dao;
using Coloc.Model;


namespace Projet_Colocation
{
    public partial class Repartition : Form
    {
        public Repartition()
        {
            InitializeComponent();
            RepartitionDepenses();
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSolder_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Voulez-vous vraiment solder les dépenses ?\nLes dépenses seront alors considérées comme réparties.", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                DaoDepense daoD = new DaoDepense();
                daoD.Solder();
                RepartitionDepenses();
            }
        }

        private void RepartitionDepenses()
        {
            lbox_Repartition.Items.Clear();
            List<Colocataire> colocataires = new DaoColocataire().GetAll();
            List<Depense> depenses = new DaoDepense().GetByNonReparti();
            decimal Total = 0;
            foreach (Depense d in depenses)
            {
                Total = Total + d.Montant;
            }
            decimal moyenne = Total / colocataires.Count();
            foreach (Colocataire c in colocataires)
            {
                List<Depense> DepenseColoc = new DaoDepense().GetByIdNonReparti(c.Id);
                decimal sommeColoc = 0;
                foreach (Depense d in DepenseColoc)
                {
                    sommeColoc = sommeColoc + d.Montant;
                }
                decimal reparti = moyenne - sommeColoc;
                string saisie = "";
                if (reparti < 0)
                {
                    saisie = "recevra";
                }
                else
                {
                    saisie = "devra régler";
                }

                lbox_Repartition.Items.Add(c.Nom + " " + c.Prenom + " a payé " + Math.Round(sommeColoc, 2) + "€, aurait du payer " + Math.Round(moyenne, 2) + "€, " + saisie + " " + Math.Abs(Math.Round(reparti, 2)) + "€");
            }
        }
    }
}
