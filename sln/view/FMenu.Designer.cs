﻿namespace Coloc.View {
    partial class FMenu {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.btnConsulterDepense = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConsulterColoc = new System.Windows.Forms.Button();
            this.btnReparti = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnConsulterDepense
            // 
            this.btnConsulterDepense.Location = new System.Drawing.Point(225, 226);
            this.btnConsulterDepense.Name = "btnConsulterDepense";
            this.btnConsulterDepense.Size = new System.Drawing.Size(288, 58);
            this.btnConsulterDepense.TabIndex = 8;
            this.btnConsulterDepense.Text = "Depense ";
            this.btnConsulterDepense.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft JhengHei", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Location = new System.Drawing.Point(144, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(460, 35);
            this.label1.TabIndex = 7;
            this.label1.Text = "Gérer les Colocataires et Dépenses";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnConsulterColoc
            // 
            this.btnConsulterColoc.Location = new System.Drawing.Point(225, 101);
            this.btnConsulterColoc.Name = "btnConsulterColoc";
            this.btnConsulterColoc.Size = new System.Drawing.Size(288, 58);
            this.btnConsulterColoc.TabIndex = 11;
            this.btnConsulterColoc.Text = "Colocataire";
            this.btnConsulterColoc.UseVisualStyleBackColor = true;
            // 
            // btnReparti
            // 
            this.btnReparti.Location = new System.Drawing.Point(225, 346);
            this.btnReparti.Name = "btnReparti";
            this.btnReparti.Size = new System.Drawing.Size(288, 58);
            this.btnReparti.TabIndex = 12;
            this.btnReparti.Text = "Repartir Depense";
            this.btnReparti.UseVisualStyleBackColor = true;
            // 
            // FMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnReparti);
            this.Controls.Add(this.btnConsulterColoc);
            this.Controls.Add(this.btnConsulterDepense);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Name = "FMenu";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnConsulterDepense;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnConsulterColoc;
        private System.Windows.Forms.Button btnReparti;
    }
}

