﻿
namespace Coloc.View
{
    partial class FModifierDepense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BtValiderModif = new System.Windows.Forms.Button();
            this.tbmodifJustificatif = new System.Windows.Forms.TextBox();
            this.tbmodifDescriptif = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbmodifMontant = new System.Windows.Forms.TextBox();
            this.tbmodifdate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Modifier Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 22);
            this.label2.TabIndex = 2;
            this.label2.Text = "Modifier Descriptif";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 22);
            this.label3.TabIndex = 4;
            this.label3.Text = " Modifier justificatif";
            // 
            // BtValiderModif
            // 
            this.BtValiderModif.Location = new System.Drawing.Point(103, 285);
            this.BtValiderModif.Name = "BtValiderModif";
            this.BtValiderModif.Size = new System.Drawing.Size(124, 43);
            this.BtValiderModif.TabIndex = 8;
            this.BtValiderModif.Text = "Valider";
            this.BtValiderModif.UseVisualStyleBackColor = true;
            // 
            // tbmodifJustificatif
            // 
            this.tbmodifJustificatif.Location = new System.Drawing.Point(204, 159);
            this.tbmodifJustificatif.Name = "tbmodifJustificatif";
            this.tbmodifJustificatif.Size = new System.Drawing.Size(119, 20);
            this.tbmodifJustificatif.TabIndex = 14;
            // 
            // tbmodifDescriptif
            // 
            this.tbmodifDescriptif.Location = new System.Drawing.Point(204, 113);
            this.tbmodifDescriptif.Name = "tbmodifDescriptif";
            this.tbmodifDescriptif.Size = new System.Drawing.Size(119, 20);
            this.tbmodifDescriptif.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(82, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(201, 28);
            this.label5.TabIndex = 11;
            this.label5.Text = "Modifier Depense";
            // 
            // tbmodifMontant
            // 
            this.tbmodifMontant.Location = new System.Drawing.Point(204, 207);
            this.tbmodifMontant.Name = "tbmodifMontant";
            this.tbmodifMontant.Size = new System.Drawing.Size(119, 20);
            this.tbmodifMontant.TabIndex = 17;
            this.tbmodifMontant.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // tbmodifdate
            // 
            this.tbmodifdate.Location = new System.Drawing.Point(179, 63);
            this.tbmodifdate.Name = "tbmodifdate";
            this.tbmodifdate.Size = new System.Drawing.Size(169, 20);
            this.tbmodifdate.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 204);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(155, 22);
            this.label6.TabIndex = 16;
            this.label6.Text = "Modifier Montant";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // FModifierDepense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 369);
            this.Controls.Add(this.tbmodifdate);
            this.Controls.Add(this.tbmodifMontant);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbmodifJustificatif);
            this.Controls.Add(this.tbmodifDescriptif);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.BtValiderModif);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FModifierDepense";
            this.Text = "ModifierDepense";
            this.Load += new System.EventHandler(this.FModifierDepense_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BtValiderModif;
        private System.Windows.Forms.TextBox tbmodifJustificatif;
        private System.Windows.Forms.TextBox tbmodifDescriptif;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbmodifMontant;
        private System.Windows.Forms.DateTimePicker tbmodifdate;
        private System.Windows.Forms.Label label6;
    }
}