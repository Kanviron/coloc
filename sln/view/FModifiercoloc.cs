﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Model;

namespace Coloc.View
{
    public partial class FModifiercoloc : Form
    {
        State state;
        ListBox.ObjectCollection items;
        int position;
        public FModifiercoloc(State state,ListBox.ObjectCollection items,int position)
        {
            InitializeComponent();
            btModifier.Click += BtModifier_Click;
            this.state = state;
            this.items = items;
            this.position = position;
            
            
            switch(state)
            {
                case State.added:
                    this.Text = "création d'une course";
                    break;
                case State.modified:
                    Colocataire coloc = (Colocataire)items[this.position];
                    this.tbModifierprenom.Text = coloc.Prenom.ToString();
                    this.tbModifierNom.Text = coloc.Nom.ToString();
                    this.tbModifieremail.Text = coloc.Mail.ToString();
                    this.tbModifiertéléphone.Text = coloc.Telephone.ToString();
                    this.Text = "Modification d'une course";
                    break;
                case State.deleted:
                    this.Text = "Supression d'une course";
                    break;
                default:
                    break;
            }
        }

        private void BtModifier_Click(object sender, EventArgs e)
        {
            switch (this.state)
            {
                case State.added:
                    items.Add(new Colocataire(0, Convert.ToString(this.tbModifierNom.Text), Convert.ToString(this.tbModifieremail.Text), Convert.ToString(this.tbModifierprenom.Text), Convert.ToInt32(this.tbModifiertéléphone.Text), this.state));
                    break;
                case State.modified:
                    Colocataire coloc = (Colocataire)items[this.position];
                    coloc.Prenom = Convert.ToString(this.tbModifierprenom.Text);
                    coloc.Nom = Convert.ToString(this.tbModifierNom.Text);
                    coloc.Mail = Convert.ToString(this.tbModifieremail.Text);
                    coloc.Telephone = Convert.ToInt32(this.tbModifiertéléphone.Text);
                    coloc.State = this.state;
                    items[this.position] = coloc;
                    this.Text = "Modification d'une course";
                    break;
                case State.deleted:
                    this.Text = "Suppression d'une course";
                    break;
                case State.unChanged:
                    this.Text = "Consultation d'un coloc";
                    break;
            }
            this.Close();
        }

    }
        
}

