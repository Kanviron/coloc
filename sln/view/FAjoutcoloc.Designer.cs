﻿
namespace Coloc.View
{
    partial class FAjoutcoloc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbAjtNom = new System.Windows.Forms.TextBox();
            this.btAjout = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbAjtprenom = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbAjtemail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbAjttéléphone = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(84, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ajouter Colocataire";
            // 
            // tbAjtNom
            // 
            this.tbAjtNom.Location = new System.Drawing.Point(236, 67);
            this.tbAjtNom.Name = "tbAjtNom";
            this.tbAjtNom.Size = new System.Drawing.Size(119, 20);
            this.tbAjtNom.TabIndex = 2;
            // 
            // btAjout
            // 
            this.btAjout.Location = new System.Drawing.Point(135, 311);
            this.btAjout.Name = "btAjout";
            this.btAjout.Size = new System.Drawing.Size(117, 37);
            this.btAjout.TabIndex = 3;
            this.btAjout.Text = "Ajouter";
            this.btAjout.UseVisualStyleBackColor = true;
            this.btAjout.Click += new System.EventHandler(this.btModif_Click_1);
            // 
            // label2
            // 
            this.label2.AccessibleRole = System.Windows.Forms.AccessibleRole.Sound;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 22);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ajouter le Nom";
            // 
            // label3
            // 
            this.label3.AccessibleRole = System.Windows.Forms.AccessibleRole.Sound;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 22);
            this.label3.TabIndex = 7;
            this.label3.Text = "Ajouter le prenom";
            // 
            // tbAjtprenom
            // 
            this.tbAjtprenom.Location = new System.Drawing.Point(237, 124);
            this.tbAjtprenom.Name = "tbAjtprenom";
            this.tbAjtprenom.Size = new System.Drawing.Size(119, 20);
            this.tbAjtprenom.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AccessibleRole = System.Windows.Forms.AccessibleRole.Sound;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(21, 240);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(155, 22);
            this.label4.TabIndex = 11;
            this.label4.Text = "Ajouter de l\'email";
            // 
            // tbAjtemail
            // 
            this.tbAjtemail.Location = new System.Drawing.Point(237, 252);
            this.tbAjtemail.Name = "tbAjtemail";
            this.tbAjtemail.Size = new System.Drawing.Size(119, 20);
            this.tbAjtemail.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AccessibleRole = System.Windows.Forms.AccessibleRole.Sound;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 179);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(176, 22);
            this.label5.TabIndex = 9;
            this.label5.Text = "Ajouter le téléphone";
            // 
            // tbAjttéléphone
            // 
            this.tbAjttéléphone.Location = new System.Drawing.Point(236, 188);
            this.tbAjttéléphone.Name = "tbAjttéléphone";
            this.tbAjttéléphone.Size = new System.Drawing.Size(119, 20);
            this.tbAjttéléphone.TabIndex = 8;
            // 
            // FAjoutcoloc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 450);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbAjtemail);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbAjttéléphone);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbAjtprenom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btAjout);
            this.Controls.Add(this.tbAjtNom);
            this.Controls.Add(this.label1);
            this.Name = "FAjoutcoloc";
            this.Text = "Modifier";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbAjtNom;
        private System.Windows.Forms.Button btAjout;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbAjtprenom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbAjtemail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbAjttéléphone;
    }
}