﻿namespace Coloc.View {
    partial class FMenu {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.btSupprimer = new System.Windows.Forms.Button();
            this.btModifier = new System.Windows.Forms.Button();
            this.btAjout = new System.Windows.Forms.Button();
            this.btConsulter = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btSupprimer
            // 
            this.btSupprimer.Location = new System.Drawing.Point(597, 187);
            this.btSupprimer.Name = "btSupprimer";
            this.btSupprimer.Size = new System.Drawing.Size(93, 40);
            this.btSupprimer.TabIndex = 11;
            this.btSupprimer.Text = "Supprimer Colocataire";
            this.btSupprimer.UseVisualStyleBackColor = true;
            // 
            // btModifier
            // 
            this.btModifier.Location = new System.Drawing.Point(338, 187);
            this.btModifier.Name = "btModifier";
            this.btModifier.Size = new System.Drawing.Size(92, 40);
            this.btModifier.TabIndex = 10;
            this.btModifier.Text = "Modifier Colocataire";
            this.btModifier.UseVisualStyleBackColor = true;
            // 
            // btAjout
            // 
            this.btAjout.Location = new System.Drawing.Point(110, 187);
            this.btAjout.Name = "btAjout";
            this.btAjout.Size = new System.Drawing.Size(93, 40);
            this.btAjout.TabIndex = 9;
            this.btAjout.Text = "Ajouter Colocataire";
            this.btAjout.UseVisualStyleBackColor = true;
            // 
            // btConsulter
            // 
            this.btConsulter.Location = new System.Drawing.Point(221, 93);
            this.btConsulter.Name = "btConsulter";
            this.btConsulter.Size = new System.Drawing.Size(298, 50);
            this.btConsulter.TabIndex = 8;
            this.btConsulter.Text = "Consulter la liste des Colocataires";
            this.btConsulter.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(284, 31);
            this.label1.TabIndex = 7;
            this.label1.Text = "Gérer les Colocataires";
            // 
            // FMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btSupprimer);
            this.Controls.Add(this.btModifier);
            this.Controls.Add(this.btAjout);
            this.Controls.Add(this.btConsulter);
            this.Controls.Add(this.label1);
            this.Name = "FMenu";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btSupprimer;
        private System.Windows.Forms.Button btModifier;
        private System.Windows.Forms.Button btAjout;
        private System.Windows.Forms.Button btConsulter;
        private System.Windows.Forms.Label label1;
    }
}

