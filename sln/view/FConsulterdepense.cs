﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Dao;
using Coloc.Model;

namespace Coloc.View
{
    public partial class FConsulterdepense : Form
    {
        int idColoc;
        public FConsulterdepense()
        {
            InitializeComponent();
            this.btModifD.Click += BtModif_Click;
            this.btSup.Click += BtSup_Click;
            this.btSave.Click += BtSave_Click;
            this.BtAjout.Click += BtAjout_Click;
            this.idColoc =0;
            this.cbDepense.SelectedIndexChanged += CbDepense_SelectedIndexChanged;
            DaoColocataire coloc = new DaoColocataire();
            loadColoc(coloc.GetAll());
            
        }

        private void CbDepense_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = cbDepense.SelectedIndex;
            int idColoc = ((Colocataire)cbDepense.Items[index]).Id;
            this.idColoc = idColoc; 
            DaoDepense depense = new DaoDepense();
            loadDepense(depense.GetAll(idColoc));
        }

        private void BtAjout_Click(object sender, EventArgs e)
        {
            FAjoutDepense fmodif = new FAjoutDepense(lbConsultationD,idColoc);
            fmodif.Show();
        }

        private void BtSave_Click(object sender, EventArgs e)
        {
            List<Depense> depenses = new List<Depense>();
            foreach (object v in lbConsultationD.Items)
            {
                depenses.Add((Depense)v);
            }
            new DaoDepense().SaveChanges(depenses);
            this.load(depenses);
        }

        private void BtSup_Click(object sender, EventArgs e)
        {
            if (lbConsultationD.SelectedIndex == -1) return;
            int position = lbConsultationD.SelectedIndex;
            ((Depense)lbConsultationD.Items[position]).Remove();
            lbConsultationD.Items[position] = lbConsultationD.Items[position];
        }

        private void BtModif_Click(object sender, EventArgs e)
        {
            if (lbConsultationD.SelectedIndex == -1) return;
            int position = lbConsultationD.SelectedIndex;
            FModifierDepense fmodif = new FModifierDepense(State.modified, lbConsultationD.Items, position);
            fmodif.Show();
        }

        private void loadDepense(List<Depense> lesdepenses)
        {
            lbConsultationD.Items.Clear();
            foreach (Depense c in lesdepenses)
            {
                lbConsultationD.Items.Add(c);
            }
        }

        private void loadColoc(List<Colocataire> lesColocataires)
        {
            cbDepense.Items.Clear();
            foreach (Colocataire c in lesColocataires)
            {
                cbDepense.Items.Add(c);
            }
        }
        private void load(List<Depense> depenses)
        {
            lbConsultationD.Items.Clear();
            foreach (Depense d in depenses)
            {
                lbConsultationD.Items.Add(d);
            }
        }


    }
}
