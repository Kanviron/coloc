﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Dao;
using Coloc.Model;

namespace Coloc.View
{
    public partial class FAjoutDepense : Form
    {
        State state;
        ListBox.ObjectCollection items;
        int position;
        ListBox lb;
        int idColoc;
        public FAjoutDepense(ListBox lb, int idColoc)
        {
            this.idColoc = idColoc;
            this.lb = lb;
            InitializeComponent();
            BtValiderDepense.Click += BtValiderDepense_Click;
        }

        private void BtValiderDepense_Click(object sender, EventArgs e)
        {
            switch (this.state)
            {
                case State.added:
                    this.lb.Items.Add(new Depense(0, Convert.ToDateTime(this.datedepense.Text), Convert.ToString(this.tbDepenseDescription.Text), Convert.ToString(this.tbDepenseJustificatif.Text), Convert.ToInt32(this.tbDepenseMontant.Text),false, this.state, this.idColoc));
                    break;
                case State.modified:
                    Depense depense = (Depense)items[this.position];
                    depense.Date = Convert.ToDateTime(this.datedepense);
                    depense.Text = Convert.ToString(this.tbDepenseDescription);
                    depense.Justificatif = Convert.ToString(this.tbDepenseJustificatif);
                    depense.Montant = Convert.ToInt32(this.tbDepenseMontant.Text);
                    depense.State = this.state;
                    items[this.position] = depense;
                    break;
                case State.deleted:
                    break;
                case State.unChanged:
                    break;
                default:
                    break;
            }
            this.Close();
        }

 

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void FAjoutDepense_Load(object sender, EventArgs e)
        {

        }
    }
}
