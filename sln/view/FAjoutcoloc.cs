﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Dao;
using Coloc.Model;

namespace Coloc.View
{
    public partial class FAjoutcoloc : Form
    {

        State state;
        ListBox.ObjectCollection items;
        int position;
        ListBox lb;
        public FAjoutcoloc(ListBox lb)
        {
            this.lb= lb;
            InitializeComponent();
            btAjout.Click += BtModif_Click;
        }

        private void BtModif_Click(object sender, EventArgs e)
        {
            switch (this.state)
            {
                case State.added:
                    this.lb.Items.Add(new Colocataire(0, Convert.ToString(this.tbAjtNom.Text), Convert.ToString(this.tbAjtprenom.Text), Convert.ToString(this.tbAjtemail.Text), Convert.ToInt32(this.tbAjttéléphone.Text), this.state));
                    break;
                case State.modified:
                    Colocataire coloc = (Colocataire)items[this.position];
                    coloc.Prenom = Convert.ToString(this.tbAjtprenom);
                    coloc.Nom = Convert.ToString(this.tbAjtNom);
                    coloc.Mail = Convert.ToString(this.tbAjtemail);
                    coloc.Telephone = Convert.ToInt32(this.tbAjttéléphone.Text);
                    coloc.State = this.state;
                    items[this.position] = coloc;
                    break;
                case State.deleted:
                    break;
                case State.unChanged:
                    break;
                default:
                    break;
            }
            this.Close();
        }

        private void btModif_Click_1(object sender, EventArgs e)
        {

        }
    }
}