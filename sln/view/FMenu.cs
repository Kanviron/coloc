﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Dao;
using Projet_Colocation;

namespace Coloc.View {
    public partial class FMenu:Form {
        public FMenu() {
            InitializeComponent();
            this.btnConsulterDepense.Click += BtnConsulterDepense_Click;
            this.btnConsulterColoc.Click += BtnConsulterColoc_Click;
            this.btnReparti.Click += BtnReparti_Click;
            DaoConnectionSingleton.SetStringConnection("root", "", "localhost", "coloc");
        }

        private void BtnReparti_Click(object sender, EventArgs e)
        {
            Repartition fReparti = new Repartition();
            fReparti.Show();
        }

        private void BtnConsulterColoc_Click(object sender, EventArgs e)
        {
            FConsultColoc FConsultColoc = new FConsultColoc();
            FConsultColoc.Show();
        }

        private void BtnConsulterDepense_Click(object sender, EventArgs e)
        {
            FConsulterdepense fConsulter = new FConsulterdepense();
            fConsulter.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
