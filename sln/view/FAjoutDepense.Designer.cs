﻿
namespace Coloc.View
{
    partial class FAjoutDepense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbDepenseJustificatif = new System.Windows.Forms.TextBox();
            this.tbDepenseDescription = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.BtValiderDepense = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.datedepense = new System.Windows.Forms.DateTimePicker();
            this.tbDepenseMontant = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbDepenseJustificatif
            // 
            this.tbDepenseJustificatif.Location = new System.Drawing.Point(208, 164);
            this.tbDepenseJustificatif.Name = "tbDepenseJustificatif";
            this.tbDepenseJustificatif.Size = new System.Drawing.Size(119, 20);
            this.tbDepenseJustificatif.TabIndex = 24;
            // 
            // tbDepenseDescription
            // 
            this.tbDepenseDescription.Location = new System.Drawing.Point(208, 118);
            this.tbDepenseDescription.Name = "tbDepenseDescription";
            this.tbDepenseDescription.Size = new System.Drawing.Size(119, 20);
            this.tbDepenseDescription.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(86, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(162, 28);
            this.label5.TabIndex = 21;
            this.label5.Text = "AjoutDepense";
            // 
            // BtValiderDepense
            // 
            this.BtValiderDepense.Location = new System.Drawing.Point(108, 324);
            this.BtValiderDepense.Name = "BtValiderDepense";
            this.BtValiderDepense.Size = new System.Drawing.Size(124, 43);
            this.BtValiderDepense.TabIndex = 20;
            this.BtValiderDepense.Text = "Valider";
            this.BtValiderDepense.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 22);
            this.label3.TabIndex = 18;
            this.label3.Text = "Ajout justificatif";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 22);
            this.label2.TabIndex = 17;
            this.label2.Text = "Ajout Descriptif";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 22);
            this.label1.TabIndex = 16;
            this.label1.Text = "Ajout Date";
            // 
            // datedepense
            // 
            this.datedepense.Location = new System.Drawing.Point(127, 70);
            this.datedepense.Name = "datedepense";
            this.datedepense.Size = new System.Drawing.Size(200, 20);
            this.datedepense.TabIndex = 26;
            // 
            // tbDepenseMontant
            // 
            this.tbDepenseMontant.Location = new System.Drawing.Point(208, 214);
            this.tbDepenseMontant.Name = "tbDepenseMontant";
            this.tbDepenseMontant.Size = new System.Drawing.Size(119, 20);
            this.tbDepenseMontant.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 212);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 22);
            this.label6.TabIndex = 28;
            this.label6.Text = "Ajout montant";
            // 
            // FAjoutDepense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 450);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbDepenseMontant);
            this.Controls.Add(this.datedepense);
            this.Controls.Add(this.tbDepenseJustificatif);
            this.Controls.Add(this.tbDepenseDescription);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.BtValiderDepense);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FAjoutDepense";
            this.Text = "FAjoutDepense";
            this.Load += new System.EventHandler(this.FAjoutDepense_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tbDepenseJustificatif;
        private System.Windows.Forms.TextBox tbDepenseDescription;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BtValiderDepense;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker datedepense;
        private System.Windows.Forms.TextBox tbDepenseMontant;
        private System.Windows.Forms.Label label6;
    }
}