﻿
namespace Coloc.View
{
    partial class FConsulterdepense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lbConsultationD = new System.Windows.Forms.ListBox();
            this.btModifD = new System.Windows.Forms.Button();
            this.btSup = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.BtAjout = new System.Windows.Forms.Button();
            this.cbDepense = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(267, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Consultation dépense";
            // 
            // lbConsultationD
            // 
            this.lbConsultationD.FormattingEnabled = true;
            this.lbConsultationD.Location = new System.Drawing.Point(27, 82);
            this.lbConsultationD.Name = "lbConsultationD";
            this.lbConsultationD.Size = new System.Drawing.Size(761, 251);
            this.lbConsultationD.TabIndex = 1;
            // 
            // btModifD
            // 
            this.btModifD.Location = new System.Drawing.Point(247, 364);
            this.btModifD.Name = "btModifD";
            this.btModifD.Size = new System.Drawing.Size(110, 49);
            this.btModifD.TabIndex = 27;
            this.btModifD.Text = "Modifier";
            this.btModifD.UseVisualStyleBackColor = true;
            // 
            // btSup
            // 
            this.btSup.Location = new System.Drawing.Point(44, 361);
            this.btSup.Name = "btSup";
            this.btSup.Size = new System.Drawing.Size(110, 49);
            this.btSup.TabIndex = 26;
            this.btSup.Text = "Supprimer";
            this.btSup.UseVisualStyleBackColor = true;
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(449, 361);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(123, 52);
            this.btSave.TabIndex = 28;
            this.btSave.Text = "Save";
            this.btSave.UseVisualStyleBackColor = true;
            // 
            // BtAjout
            // 
            this.BtAjout.Location = new System.Drawing.Point(655, 364);
            this.BtAjout.Name = "BtAjout";
            this.BtAjout.Size = new System.Drawing.Size(133, 46);
            this.BtAjout.TabIndex = 35;
            this.BtAjout.Text = "Ajout";
            this.BtAjout.UseVisualStyleBackColor = true;
            // 
            // cbDepense
            // 
            this.cbDepense.FormattingEnabled = true;
            this.cbDepense.Location = new System.Drawing.Point(27, 55);
            this.cbDepense.Name = "cbDepense";
            this.cbDepense.Size = new System.Drawing.Size(761, 21);
            this.cbDepense.TabIndex = 36;
            // 
            // FConsulterdepense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cbDepense);
            this.Controls.Add(this.BtAjout);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.btModifD);
            this.Controls.Add(this.btSup);
            this.Controls.Add(this.lbConsultationD);
            this.Controls.Add(this.label1);
            this.Name = "FConsulterdepense";
            this.Text = "Consulter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbConsultationD;
        private System.Windows.Forms.Button btModifD;
        private System.Windows.Forms.Button btSup;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Button BtAjout;
        private System.Windows.Forms.ComboBox cbDepense;
    }
}