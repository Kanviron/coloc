﻿
namespace Coloc.View
{
    partial class FConsultColoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dd = new System.Windows.Forms.Label();
            this.lbConsultColoc = new System.Windows.Forms.ListBox();
            this.btSupprimer = new System.Windows.Forms.Button();
            this.btAjout = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.btnModifier = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dd
            // 
            this.dd.AutoSize = true;
            this.dd.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dd.Location = new System.Drawing.Point(245, 9);
            this.dd.Name = "dd";
            this.dd.Size = new System.Drawing.Size(292, 26);
            this.dd.TabIndex = 0;
            this.dd.Text = "Consultation des colocataires";
            // 
            // lbConsultColoc
            // 
            this.lbConsultColoc.FormattingEnabled = true;
            this.lbConsultColoc.Location = new System.Drawing.Point(45, 52);
            this.lbConsultColoc.Name = "lbConsultColoc";
            this.lbConsultColoc.Size = new System.Drawing.Size(676, 381);
            this.lbConsultColoc.TabIndex = 1;
            // 
            // btSupprimer
            // 
            this.btSupprimer.Location = new System.Drawing.Point(78, 440);
            this.btSupprimer.Name = "btSupprimer";
            this.btSupprimer.Size = new System.Drawing.Size(132, 34);
            this.btSupprimer.TabIndex = 10;
            this.btSupprimer.Text = "Suprimmer";
            this.btSupprimer.UseVisualStyleBackColor = true;
            // 
            // btAjout
            // 
            this.btAjout.Location = new System.Drawing.Point(414, 439);
            this.btAjout.Name = "btAjout";
            this.btAjout.Size = new System.Drawing.Size(132, 35);
            this.btAjout.TabIndex = 20;
            this.btAjout.Text = "Ajouter";
            this.btAjout.UseVisualStyleBackColor = true;
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(250, 440);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(132, 34);
            this.btSave.TabIndex = 21;
            this.btSave.Text = "Save";
            this.btSave.UseVisualStyleBackColor = true;
            // 
            // btnModifier
            // 
            this.btnModifier.Location = new System.Drawing.Point(573, 439);
            this.btnModifier.Name = "btnModifier";
            this.btnModifier.Size = new System.Drawing.Size(132, 35);
            this.btnModifier.TabIndex = 22;
            this.btnModifier.Text = "Modifier";
            this.btnModifier.UseVisualStyleBackColor = true;
            // 
            // FConsultColoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 497);
            this.Controls.Add(this.btnModifier);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.btAjout);
            this.Controls.Add(this.btSupprimer);
            this.Controls.Add(this.lbConsultColoc);
            this.Controls.Add(this.dd);
            this.Name = "FConsultColoc";
            this.Text = "FConsultColoc";
            this.Load += new System.EventHandler(this.FConsultColoc_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label dd;
        private System.Windows.Forms.ListBox lbConsultColoc;
        private System.Windows.Forms.Button btSupprimer;
        private System.Windows.Forms.Button btAjout;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Button btnModifier;
    }
}