﻿using Coloc.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coloc.View
{
    public partial class FModifierDepense : Form
    {
        State state;
        ListBox.ObjectCollection items;
        int position;


        public FModifierDepense(State state, ListBox.ObjectCollection items, int position)
        {
            InitializeComponent();
            this.BtValiderModif.Click += BtValiderModif_Click;
            this.state = state;
            this.items = items;
            this.position = position;

            switch (state)
            {
                case State.added:
                    this.Text = "création d'une course";
                    break;
                case State.modified:
                    Depense Depense = (Depense)items[this.position];
                    this.tbmodifdate.Text = Depense.Date.ToString();
                    this.tbmodifDescriptif.Text = Depense.Text.ToString();
                    this.tbmodifJustificatif.Text = Depense.Justificatif.ToString();
                    this.tbmodifMontant.Text = Depense.Montant.ToString();
                    this.Text = "Modification d'une course";
                    break;
                case State.deleted:
                    this.Text = "Supression d'une course";
                    break;
                default:
                    break;
            }
        }



        private void BtValiderModif_Click(object sender, EventArgs e)
        {
            switch (this.state)
            {
                case State.added:
                    items.Add(new Depense(0, Convert.ToDateTime(this.tbmodifdate), Convert.ToString(this.tbmodifDescriptif.Text), Convert.ToString(this.tbmodifJustificatif.Text), Convert.ToInt32(this.tbmodifMontant.Text), false, this.state,0));
                    break;
                case State.modified:
                    Depense Depense = (Depense)items[this.position];
                    Depense.Date = Convert.ToDateTime(this.tbmodifdate.Text);
                    Depense.Text = Convert.ToString(this.tbmodifDescriptif.Text);
                    Depense.Justificatif = Convert.ToString(this.tbmodifJustificatif.Text);
                    Depense.Montant = Convert.ToInt32(this.tbmodifMontant.Text);
                    Depense.State = this.state;
                    items[this.position] = Depense;
                    this.Text = "Modification d'une course";
                    break;
                case State.deleted:
                    this.Text = "Suppression d'une course";
                    break;
                case State.unChanged:
                    this.Text = "Consultation d'un coloc";
                    break;
            }
            this.Close();
        }

        private void FModifierDepense_Load(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
