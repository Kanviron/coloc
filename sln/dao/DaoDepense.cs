﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Coloc.Model;
using MySql.Data.MySqlClient;

namespace Coloc.Dao
{
    public class DaoDepense
    {
        public void SaveChanges(List<Depense> depenses)
        {
            for (int i = 0; i < depenses.Count; i++)
            {
                Depense depense = depenses[i];
                switch (depense.State)
                {
                    case State.modified:
                        this.update(depense);
                        break;
                    case State.added:
                        this.insert(depense);
                        break;
                    case State.deleted:
                        this.delete(depense);
                        depenses.Remove(depense);
                        break;
                }
            }
        }

        private void delete(Depense depense)
        {
            using (MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection())
            {
                cnx.Open();
                using (MySqlCommand cmd = new MySqlCommand("delete from Depense where id=@id", cnx))
                {
                    cmd.Parameters.Add(new MySqlParameter("@id", MySqlDbType.Int32));
                    cmd.Parameters["@id"].Value = depense.Id;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void update(Depense depense)
        {
            using (MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection())
            {
                cnx.Open();
                using (MySqlCommand cmd = new MySqlCommand("update Depense set DateDepense=@date,text=@text,justificatif=@justificatif,montant=@montant,reparti=@reparti where id=@id", cnx))
                {
                    cmd.Parameters.Add(new MySqlParameter("@date", MySqlDbType.Date));
                    cmd.Parameters.Add(new MySqlParameter("@text", MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@justificatif", MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@montant", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@reparti", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@id", MySqlDbType.Int32));
                    cmd.Parameters["@id"].Value = depense.Id;
                    cmd.Parameters["@date"].Value = depense.Date;
                    cmd.Parameters["@text"].Value = depense.Text;
                    cmd.Parameters["@justificatif"].Value = depense.Justificatif;
                    cmd.Parameters["@montant"].Value = depense.Montant;
                    cmd.Parameters["@reparti"].Value = depense.Reparti;
                    
                    cmd.ExecuteNonQuery();
                }
            }
            depense.State = State.unChanged;
        }

        public List<Depense> GetAll(int idcoloc)
        {
            List<Depense> depense = new List<Depense>();
            using (MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection())
            {
                cnx.Open();
                using (MySqlCommand cmd = new MySqlCommand("select id,datedepense,text,justificatif,montant,reparti from Depense where idColocataire = @idcoloc", cnx))
                {
                    cmd.Parameters.Add(new MySqlParameter("@idcoloc", MySqlDbType.Int32));
                    cmd.Parameters["@idcoloc"].Value = idcoloc;
                    using (MySqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            depense.Add(new Depense(Convert.ToInt32(rdr["id"]), Convert.ToDateTime(rdr["datedepense"]), Convert.ToString(rdr["text"]), Convert.ToString(rdr["justificatif"]), Convert.ToInt32(rdr["montant"]), Convert.ToBoolean(rdr["reparti"]), State.unChanged,idcoloc));
                        }
                    }
                }
            }
            return depense;
        }

        private void insert(Depense depense)
        {
            using (MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection())
            {
                cnx.Open();
                using (MySqlCommand cmd = new MySqlCommand("insert into Depense(Datedepense,text,justificatif,montant,reparti,idColocataire) values(@date,@text,@justificatif,@montant,@reparti,@idcoloc)", cnx))
                {
                    cmd.Parameters.Add(new MySqlParameter("@date", MySqlDbType.Date));
                    cmd.Parameters.Add(new MySqlParameter("@text", MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@justificatif", MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@montant", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@reparti", MySqlDbType.Decimal));
                    cmd.Parameters.Add(new MySqlParameter("@idcoloc", MySqlDbType.Int32));
                    cmd.Parameters["@idcoloc"].Value = depense.Idcoloc;
                    cmd.Parameters["@date"].Value = depense.Date;
                    cmd.Parameters["@text"].Value = depense.Text;
                    cmd.Parameters["@justificatif"].Value = depense.Justificatif;
                    cmd.Parameters["@montant"].Value = depense.Montant;
                    cmd.Parameters["@reparti"].Value = depense.Reparti;
                    cmd.ExecuteNonQuery();
                    // Todo gérer LastInsertId
                }
            }
            depense.State = State.unChanged;
        }

        public void Solder()
        {
            using (MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection())
            {
                cnx.Open();
                using (MySqlCommand cmd = new MySqlCommand("UPDATE Depense SET reparti = 1 where reparti = 0;", cnx))
                {
                    cmd.ExecuteNonQuery();
                }
                cnx.Close();
            }
        }

        public List<Depense> GetByNonReparti()
        {
            List<Depense> Depenses = new List<Depense>();
            using (MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection())
            {
                cnx.Open();
                using (MySqlCommand cmd = new MySqlCommand("select id, datedepense, text, justificatif, montant, reparti, idColocataire from Depense where reparti = 0", cnx))
                {
                    using (MySqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Depenses.Add(new Depense(Convert.ToInt32(rdr["id"]), Convert.ToDateTime(rdr["datedepense"]), Convert.ToString(rdr["text"]), Convert.ToString(rdr["justificatif"]), Convert.ToInt32(rdr["montant"]), Convert.ToBoolean(rdr["reparti"]), State.unChanged, Convert.ToInt32(rdr["idColocataire"])));
                        }
                    }
                }
                cnx.Close();
            }
            return Depenses;
        }

        public List<Depense> GetByIdNonReparti(int id)
        {
            List<Depense> Depenses = new List<Depense>();
            using (MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection())
            {
                cnx.Open();
                using (MySqlCommand cmd = new MySqlCommand("select id, datedepense, text, justificatif, montant, reparti, idColocataire from depense where idColocataire = @idColocataire AND reparti = 0", cnx))
                {
                    cmd.Parameters.Add(new MySqlParameter("@idColocataire", MySqlDbType.Int32));
                    cmd.Parameters["@idColocataire"].Value = id;
                    using (MySqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Depenses.Add(new Depense(Convert.ToInt32(rdr["id"]), Convert.ToDateTime(rdr["datedepense"]), Convert.ToString(rdr["text"]), Convert.ToString(rdr["justificatif"]), Convert.ToInt32(rdr["montant"]), Convert.ToBoolean(rdr["reparti"]), State.unChanged, Convert.ToInt32(rdr["idColocataire"])));
                        }
                    }
                }
                cnx.Close();
            }
            return Depenses;
        }

    }
}

