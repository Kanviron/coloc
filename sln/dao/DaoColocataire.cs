﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Coloc.Model;
using MySql.Data.MySqlClient;

namespace Coloc.Dao
{
    public class DaoColocataire
    {
        public void SaveChanges(List<Colocataire> colocs)
        {
            for (int i = 0; i < colocs.Count; i++)
            {
                Colocataire coloc = colocs[i];
                switch (coloc.State)
                {
                    case State.modified:
                        this.update(coloc);
                        break;
                    case State.added:
                        this.insert(coloc);
                        break;
                    case State.deleted:
                        this.delete(coloc);
                        colocs.Remove(coloc);
                        break;
                }
            }
        }

        private void delete(Colocataire coloc)
        {
            using (MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection())
            {
                cnx.Open();
                using (MySqlCommand cmd = new MySqlCommand("delete from Colocataire where id=@id", cnx))
                {
                    cmd.Parameters.Add(new MySqlParameter("@id", MySqlDbType.Int32));
                    cmd.Parameters["@id"].Value = coloc.Id;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void update(Colocataire coloc)
        {
            using (MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection())
            {
                cnx.Open();
                using (MySqlCommand cmd = new MySqlCommand("update Colocataire set nom=@nom,prenom=@prenom,telephone=@telephone,mail=@mail where id=@id", cnx))
                {
                    cmd.Parameters.Add(new MySqlParameter("@nom", MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@prenom", MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@telephone", MySqlDbType.Int32));
                    cmd.Parameters.Add(new MySqlParameter("@mail", MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@id", MySqlDbType.Int32));
                    cmd.Parameters["@id"].Value = coloc.Id;
                    cmd.Parameters["@nom"].Value = coloc.Nom;
                    cmd.Parameters["@prenom"].Value = coloc.Prenom;
                    cmd.Parameters["@telephone"].Value = coloc.Telephone;
                    cmd.Parameters["@mail"].Value = coloc.Mail;
                    cmd.ExecuteNonQuery();
                }
            }
            coloc.State = State.unChanged;
        }

        public List<Colocataire> GetAll()
        {
            List<Colocataire> coloc = new List<Colocataire>();
            using (MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection())
            {
                cnx.Open();
                using (MySqlCommand cmd = new MySqlCommand("select id,prenom,nom,mail,telephone from Colocataire", cnx))
                {
                    using (MySqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            coloc.Add(new Colocataire(Convert.ToInt32(rdr["id"]), Convert.ToString(rdr["prenom"]), Convert.ToString(rdr["nom"]), Convert.ToString(rdr["mail"]), Convert.ToInt32(rdr["telephone"]), State.unChanged));
                        }
                    }
                }
            }
            return coloc;
        }

        private void insert(Colocataire coloc)
        {
            using (MySqlConnection cnx = DaoConnectionSingleton.GetMySqlConnection())
            {
                cnx.Open();
                using (MySqlCommand cmd = new MySqlCommand("insert into Colocataire(prenom,nom,mail,telephone) values(@prenom,@nom,@mail,@telephone)", cnx))
                {
                    cmd.Parameters.Add(new MySqlParameter("@Prenom", MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@nom", MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@mail", MySqlDbType.VarChar));
                    cmd.Parameters.Add(new MySqlParameter("@telephone", MySqlDbType.Int32));
                    cmd.Parameters["@nom"].Value = coloc.Nom;
                    cmd.Parameters["@prenom"].Value = coloc.Prenom;
                    cmd.Parameters["@mail"].Value = coloc.Mail;
                    cmd.Parameters["@telephone"].Value = coloc.Telephone;
                    cmd.ExecuteNonQuery();
                    // Todo gérer LastInsertId
                }
            }
            coloc.State = State.unChanged;
        }

    }
}

