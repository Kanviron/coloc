﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coloc.Model
{
    public class Colocataire
    {
        private int id;
        private string prenom;
        private string nom;
        private string mail;
        private int telephone;
        private State state;
        public Colocataire(int id,string prenom,string nom,string mail,int telephone, State state)
        {
            this.id = id;
            this.prenom = prenom;
            this.nom = nom;
            this.mail = mail;
            this.telephone = telephone;
            this.state = state;
        }

        public int Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        public string Prenom
        {
            get
            {
                return this.prenom;
            }
            set
            {
                this.prenom = value;
            }
        }

        public void Remove()
        {
            this.state = State.deleted;
        }
        public State State
        {
            get
            {
                return this.state;
            }
            set
            {
                this.state = value;
            }
        }

        public string Nom
        {
            get
            {
                return this.nom;
            }
            set
            {
                this.nom = value;
            }
        }

        public string Mail
        {
            get
            {
                return this.mail;
            }
            set
            {
                this.mail = value;
            }
        }

        public int Telephone
        {
            get
            {
                return this.telephone;
            }
            set
            {
                this.telephone = value;
            }
        }


        public override string ToString()
        {
            return String.Format("Le colocataire se nomme {0} {1}, son mail est {2} et son num est  {3} et son etat est {4}", this.prenom, this.nom, this.mail, this.telephone,this.state) ;
        }

    }
}
