﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coloc.Model
{
    public class Depense
    {
        private int id;
        private DateTime date;
        private string text;
        private string justificatif;
        private int montant;
        private bool reparti;
        private State state;
        private int idcoloc;

        public Depense(int id,DateTime date,string text, string justificatif,int montant,bool reparti,State state,int idcoloc)
        {
            this.id = id;
            this.date = date;
            this.text = text;
            this.justificatif = justificatif;
            this.montant = montant;
            this.state = state;
            this.reparti = reparti;
            this.Idcoloc = idcoloc;
        }

        public bool Reparti
        {
            get
            {
                return this.reparti;
            }
            set
            {
                this.reparti = value;
            }
        }
        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                this.text = value;
            }
        }
        public int Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        public void Remove()
        {
            this.state = State.deleted;
        }

        public int Montant
        {
            get
            {
                return this.montant;
            }
            set
            {
                this.montant = value;
            }
        }
        public State State
        {
            get
            {
                return this.state;
            }
            set
            {
                this.state = value;
            }
        }

        public string Justificatif
        {
            get
            {
                return this.justificatif;
            }
            set
            {
                this.justificatif = value;
            }
        }
        public DateTime Date
        {
            get
            {
                return this.date;
            }
            set
            {
                this.date = value;
            }
        }

        public int Idcoloc { get => idcoloc; set => idcoloc = value; }


        public override string ToString()
        {
            return String.Format("Le depense date de {0}, la description : {1} , justificatif: {2} , le montant est de {3} , la repartition est {4} et l'état est {5} ", this.date, this.text, this.justificatif, this.montant, this.reparti,this.state);
        }
    }
}
