# coloc

## 1. Contexte

Le C.R.O.U.S souhaite proposer aux étudiants qui partagent une résidence en colocation un logiciel leur permettant de
de répartir leurs dépenses et d'ainsi les aider à gérer leur budget.

Ce logiciel sera proposé en téléchargement sur leur site.

![crousLogo.png](./img/crousLogo.png)
![crousBandeau.png](./img/crousBandeau.png)
![crous.png](./img/crous.png)

```plantuml
'skinparam linetype ortho  
'skinparam linetype polyline
left to right direction
:Etudiant: as Etudiant
package Colocataire {
    Etudiant --- (Télécharge l'application Colocataire)
    Etudiant --- (Installle l'application)
    Etudiant --- (Gére ses dépenses)
}
```

## 2. Conteneurs : outils mis en oeuvre

> c#, windowsForm, mysql(sqllite plus adapté pour ce genre de micro application), git, ado.net.

## 3. Les différents composants logiciels développés

### 3.1. Base de données

```plantuml
    class Depense {
        <u>id</u>:unsignedint[11]
        date:DateTime
        text:varchar[50]
        justificatif:varchar[256]
        montant:numeric[10,2]
        reparti bool
    }
    class Colocataire {
        <u>id</u>
        nom:varchar[50]
        prenom:varchar[50]
        mail:varchar[50]
        telephone:varchar[20]
    }

Depense "*" -r- "1" Colocataire : effectuer
```

### 3.2. Enregistrer une dépense

```plantuml
left to right direction
:un colocataire: as colocataire
package EnregistrerUneDepense {
    colocataire -- (Consulter les dépenses)
    colocataire -- (Saisir une dépense)
    colocataire -- (Modifier une dépense)
    colocataire -- (Supprimer une dépense)
    (Saisir une dépense) <.. (sélectionne un fichier) : <<extends>>
    (Modifier une dépense) <.. (sélectionne un fichier) : <<extends>>
}
```

<u>use case : Saisir une dépense</u>

1. Le colocataire crée une nouvelle dépense
2. Il renseigne la date à laquelle cette dépense à été effectuée
3. Il saisit un descriptif de cette dépense
4. Il a la possibilité de joindre un justificatif (use case Sélectionne un fichier).
5. Il saisit le montant
6. Il précise quel colocataire a effectué la dépense.

<u>use case : Sélectionne un fichier</u>

1. L'application propose un explorateur de fichiers (type windows)
2. L'utilisateur navigue dans l'arborescence des répertoires et sélectionner un fichier

Seule une sélection d'extensions de fichiers images sont visibles : jpg, jpeg, png, pdf, etc.

Use case : Modifier une dépense et use case : Supprimer une dépense

N'est possible que si la dépense n'a pas été répartie

### 3.3. Mettre en répartition les dépenses d'une période

```plantuml
left to right direction
:colocataire: as colocataire
package RepartirDepense {
    colocataire -- (Lancer la répartition)
    (Lancer la répartition) ..> (Visualiser le résultat de la répartition):<<include>>
    (Lancer la répartition)..>(Calculer la répartition):<<include>>
}
```

### 3.4. Solder une période

```plantuml
left to right direction
:colocataire: as colocataire
package SolderPeriode {
    colocataire -- (Solder une période)
    (Solder une période) ..> (Lancer la répartition):<<include>>
    (Solder une période)..>(Les dépenses sont déffinis comment étant répartis):<<include>>
}
```

### 3.5. Gérer les colocataires

```plantuml
left to right direction
:un colocataire: as colocataire
package GérerColocataire {
    colocataire -- (Consulter la liste des Colocataires)
    colocataire -- (Ajouter Colocataire)
    colocataire -- (Modifier Colocataire)
    colocataire -- (Supprimer Colocataire)
}
```

### 3.6. Cas d'utilisation Calculer la répartition

Permet de calculer la répartion des sommes dépensées.

1. On part de liste des dépenses non réparties
![depenses.png](img/depenses.png)
2. Les dépenses sont cumulées.
3. Les dépenses cumulées sont divisées par le nombre de colocataires.
4. Les dépenses faites par colocataires sont cumulées.
5. On fait la différence entre ce qu'il aurait du payé et ce qu'il a réellement payé.
![repartition.png](img/repartition.png)
6. A l'issue de la répartition chacun paye ou reçoit le solde de la répartition (cette partie n'est pas prise en compte dans l'application)
7. Les dépenses sont définies comment étant réparties.
8. Les soldes sont remis à zéro
9. On repart sur une nouvelle période

## 4. Diagrammes de Classe

### 4.1. Classes métier

```plantuml
skinparam classAttributeIconSize 0
class DepensesNonReparties {
    +MontantDu():decimal
    +TotalDesDepenses(int countColocataire):decimal
} 

class Depense {
    -id:int
    -date:DateTime
    -text:string
    -justificatif:string
    -montant:decimal
    -reparti:bool
    +Depense(int id, DateTime date,string,descriptif,string justificatif, +decimal montant, bool reparti)
}

class Colocataires {
    + Count()
}

class Colocataire {
    -id:int
    -nom:string
    -prenom:string
    -mail:string
    -telephone:string
    +APaye():decimal
    +Doit(decimal montantDu)
}

DepensesNonReparties-->"*"Depense:-lesDepenses
Colocataires-l->"*"Colocataire
Colocataire"-qui 1"--"*"Depense:effectuer
```

### 4.2. Description des méthodes

|Classe|Méthode|Description|
|:-:|-|-|
|Colocataire|APaye|Montant total des dépenses effectuées par un colocataire|
|-|Doit|Différence entre le montant du par chaque colocataire et le montant des dépenses effectuées par celui-ci|
|Colocataires|Count|Donne le nombre de colocataires|
|DepensesNonRéparties|MontantDu| Montant total des dépenses divisé par le nombre de colocataires|
|-| TotalDesDepenses |Montant total des dépenses à répartir|
|Depense| | |

### 4.3. Classes d'accès aux données
